<?php

namespace Drupal\simple_content\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Simple content entities.
 *
 * @ingroup simple_content
 */
class SimpleContentDeleteForm extends ContentEntityDeleteForm {


}
