<?php

namespace Drupal\simple_content\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Simple content type entities.
 */
interface SimpleContentTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
